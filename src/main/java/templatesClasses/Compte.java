package templatesClasses;

import java.util.Date;

public class Compte
{

    public Compte(){
        // default constructior
    }
    public Compte(Date data1, String libelle, Integer montant, Boolean typeBC, Boolean sensDC) {
        this.data1 = data1;
        this.libelle = libelle;
        this.montant = montant;
        this.typeBC = typeBC;
        this.sensDC = sensDC;
    }



    public Date data1;
    public String libelle;
    public Integer montant;
    // type banque
    public Boolean typeBC;
    // debit = True
    public Boolean sensDC;

}

