package lectureFile;
/*

import org.dhatim.fastexcel.Workbook;
import org.dhatim.fastexcel.Worksheet;
*/

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.apache.poi.hssf.record.aggregates.RowRecordsAggregate.createRow;

public class MetodsOutInp implements IMetodsOutInp {


    public int i=0;
    public int j=0;
    // Default constructor
    public MetodsOutInp(){
        // nothing
    }

    @Override
    public void outputFileDat(String f) {
        System.out.println("in this subroutine is output .DAT");
    }

    @Override
    public void writeFileExcel(String fDir) {
        // position fail Excel.xlsx
        String f = new File(fDir).getAbsolutePath();

        try  {
            System.out.println(f);

            XSSFWorkbook wb =new XSSFWorkbook();

            XSSFSheet ws = wb.createSheet("sheet3");

            Object empdata[][]={
                    {"EmpID","Empnik"},
                    {101,"Ismail"},
                    {102,"Rama"},
                    {103,"Bharata"},
                    {145,"Mohammed3"}
            };

            int limSupi = empdata.length; // lsi : 4
            int limSupj = empdata[0].length; // lsj : 2
            System.out.println(limSupj + " ____ " + limSupi);
            i=0;

            do{
                System.out.println(i);
                // instance  i-eme rows
                XSSFRow iRow = ws.createRow(i);
                j=0;
                do{
                    // create cell for j-eme columns
                    XSSFCell cell = iRow.createCell(j);

                    System.out.println(cell.getRawValue());

                    Object value = empdata[i][j];

                    // casting

                    if(value instanceof String){
                        cell.setCellValue((String) value);
                    }
                    if(value instanceof Boolean){
                        cell.setCellValue((Boolean) value);
                    }
                    if(value instanceof Integer){
                        cell.setCellValue((Integer)value);
                    }
                    if(value instanceof Double){
                        cell.setCellValue((Double) value);
                    }
                    if(empdata[i][j] instanceof Date){
                        cell.setCellValue((Date) value);
                    }


                    j=j+1;
                }while(j<limSupj);
                i=i+1;
            }while(i<limSupi);

            // note ; after annignement all data, outPut in Data Stream File
            FileOutputStream outputStream = new FileOutputStream(f);
            System.out.println(outputStream);
            wb.write(outputStream);
/*
            Workbook wb = new Workbook(fStream, "Microsoft Excel", "1.0");

            Worksheet ws = wb.newWorksheet("Book1");
            String srt1 = "inputStream";
            ws.value(1,1, LocalDateTime.now());
            ws.value(1,2, srt1);
            ws.value(1,3, 545);
            System.out.println("in this subroutine write File Excel");
            wb.finish();*/
        }catch (Exception e){
            System.out.println("exception fouded in subrouitne writeFileExcel"+e);
        }finally {
            System.out.println("coorectly finished subroutine output fileWriter");
        }
    }

    @Override
    public void readFileExcel(String f) {
        System.out.println("in this subroutine is read Excel Data");
        try {
            FileInputStream fileInputStream = new FileInputStream(f);
            XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
            XSSFSheet ws =wb.getSheet("Sheet1");

            int limSupi=ws.getLastRowNum();  // this is the limSup for Row
            // and this is the limSup of Col
            int limSupj=ws.getRow(0).getLastCellNum(); // getFirstCellNum();
            i=0;
            do{
                // get interely line record
                XSSFRow row = ws.getRow(i);
                // System.out.println(row);

                j=0;
                do {
                    //
                    XSSFCell cellExcel = row.getCell(j);
                    // make if else maybe ?
                    // TODO : assignement to Array for calcule in Subroutine
                    // function assignemnt assignemnt array(A[], Type CellExcel.getCellType());
                    switch(cellExcel.getCellType()){
                        case STRING:
                            // one only function for assignement
                            System.out.println(cellExcel.getStringCellValue());
                            break;
                        case NUMERIC:
                            System.out.println(cellExcel.getNumericCellValue());
                            break;
                        case BOOLEAN:
                            System.out.println(cellExcel.getBooleanCellValue());
                            break;
                       /* case:
                            System.out.println(cellExcel.getSheet());
                            break;*/
                    }
                    System.out.println(":");
                    j=j+1;
                }while(j>limSupj);

                i=i+1;
            }while(i<limSupi);

            // TODO

        }catch (Exception e){
            System.out.println(e+"_:_eexception foundent in file read data");
        }
    }
}
