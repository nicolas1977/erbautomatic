package lectureFile;

public interface IMetodsOutInp {
    void outputFileDat(String f);
    void writeFileExcel(String f);
    void readFileExcel(String f);
}
